### api

1. 复制接口教程中的 odoojs 文件夹
2. 创建文件 api/index.js

```
import { ODOO } from '@/odoojs'

const baseURL = process.env.VUE_APP_BASE_API

export const api = new ODOO({ baseURL })

export default api

```

#### 修改 views/home/index.vue

1. 模版部分

```
    <div>odoo version: {{ api.version_info.server_version }}</div>
    <div>odoo version: {{ version_info.server_version }}</div>
```

2. 代码部分

```
import api from '@/api'

export default {
  name: 'Home',
  components: {},
  mixins: [],

  data() {
    return {
      api,
      version_info: {}
    }
  },
  computed: {},
  async created() {
    this.version_info = await api.version_info_promise
  },

  methods: {
    onLogout() {
      this.$router.push({ path: '/user/login' })
    }
  }
}

```

3. 以后访问接口, 都是通过 api 这个对象
4. 现在页面中已经可以渲染 odoo 的版本号

![首页面](https://gitee.com/odoowww/odoorpc-js-tutorial/raw/master/docs/tutorial-web/image/01.home.jpg '首页面')

### 财务报表

1. 本节描述 财务报表的展示和 excel 导出
2. 直接 从 odoo 中, 获取报表的 JSON 数据, 在前端展示
3. 将报表数据 以 excel 格式 导出到本地

### 财务报表 的 odoo 原生处理

1. 需要安装第三方模块 om_account_accountant
2. 该模块提供总账, 应收应付账, 余额表和损益表
3. odoo 的所有报表以 qweb 格式直接在页面展示. 若配置 pdf 则以 pdf 文件导出

### 设计思路

1. 模型 account.common.report 是所有报表的基础
2. 该模型中有函数 \_get_report_values, 获取报表数据
3. 以下划线开头的函数无法通过 JSONRPC 方式直接调用
4. 因此需要封装一个函数, 供 JSONRPC 访问
5. odoo.addons.web.controllers.main.ExcelExport 是 Excel 导出控制模块
6. 基于 ExcelExport 扩展一个新接口, 用于 财务报表的导出

### odoo 报表基础模块开发

1. 定义新模块 account_report_print
2. 模型 account.common.report 中定义新函数 export_report_month
3. export_report_month 的输入参数为 报表名称, 月份, 及报表参数
4. export_report_month 调用 \_get_report_values, 获取数据
5. 函数 export_report_month, 对外开放, 可以被 JSONRPC 访问
6. 定义新函数 search_read_months, 获取当前财务数据中的所有月份
7. 定义新接口 /web2/export/xlsx/account/report
8. 接口参数为 报表名称, 月份, 及报表参数
9. 接口首先调用 export_report_month, 获取数据
10. 将数据组织为 excel 文件格式, 供前端下载读取

### odoo 财务报表模块开发

1. 定义新模块 accounting_pdf_reports_patch
2. 各财务报表均继承自 account.common.report
3. 各财务报表 重写 export_report_month 函数, 补充 fields 信息
4. 部分财务报表, 在重写的 export_report_month 函数中, 补充期初期末值
5. 解决报表 report.accounting_pdf_reports.report_partnerledger 的函数 \_get_report_values 返回值的计算问题
6. account_financial_report_balancesheet0 报表结构调整, 拆分为资产负债权益三类, 并将损益表的结果做为权益的子项
7. account_financial_report_balancesheet0 结果中包括期初期末及本期值
8. 模型 account.report.partner.ledger, 返回结果中, 补充期初期末值

### 财务报表的 odoorpc 处理

1. 调用模型方法 search_read_months, 获取报表月份列表
2. 调用模型方法 export_report_month, 获取报表数据
3. 在报表数据中, fields 为数据的字段结构. lines 数据行
4. 前端中直接使用 fields 和 lines, 渲染数据
5. 调用接口 /web2/export/xlsx/account/report, 下载导出报表的 Excel 文件
